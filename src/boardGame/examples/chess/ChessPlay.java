package boardGame.examples.chess;

import boardGame.examples.chess.model.CheckerSquare;

public class ChessPlay {

	private CheckerSquare from;
	private CheckerSquare to;
	
	public ChessPlay(CheckerSquare from, CheckerSquare to) {
		super();
		this.from = from;
		this.to = to;
	}

	public CheckerSquare getFrom() {
		return from;
	}

	public void setFrom(CheckerSquare from) {
		this.from = from;
	}

	public CheckerSquare getTo() {
		return to;
	}

	public void setTo(CheckerSquare to) {
		this.to = to;
	}
}
