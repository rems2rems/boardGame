package boardGame.examples.chess.model;

import boardGame.api.model.Direction;

public enum Color {

	WHITE(Direction.UP),BLACK(Direction.DOWN);

	private Direction direction;
	
	public Direction getDirection() {
		return direction;
	}

	private Color(Direction direction) {
		this.direction = direction;
	}
	
	public Color getOpposite() {
		if(this == WHITE) {
			return BLACK;
		}
		return WHITE;
	}
}
