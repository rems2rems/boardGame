package boardGame.examples.chess.model.pieces;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import boardGame.api.model.Direction;
import boardGame.api.model.Square;
import boardGame.examples.chess.model.CheckerSquare;
import boardGame.examples.chess.model.Color;
import boardGame.examples.chess.model.Piece;
import boardGame.examples.chess.model.Type;

public class Pawn extends Piece {

	public Pawn(Color color) {
		super(Type.PAWN, color);
	}

	@Override
	public Map<Direction,Set<CheckerSquare>> getAttacksByDirection() {
		
		Map<Direction,Set<CheckerSquare>> attacks = new HashMap<>();
		addSideAttack(attacks, Direction.RIGHT);
		addSideAttack(attacks, Direction.LEFT);
			
		return attacks;
	}

	public void addSideAttack(Map<Direction, Set<CheckerSquare>> attacks, Direction currentDir) {
		try {
			Set<CheckerSquare> moves = new HashSet<>();
			Direction direction = color.getDirection().combineWith(currentDir);
			CheckerSquare square = (CheckerSquare)getSquare().getNeighbor(direction);
			if (square.hasPawn() && square.getPiece().getColor() == color.getOpposite()) {
				moves.add(square);
				attacks.put(direction,moves);
			}
		} catch (Exception e) {
			
		}
		// TODO 1 : en passant...
		if(getRowPosition()>=5) {
			
			try {
				CheckerSquare nei = (CheckerSquare)getSquare().getNeighbor(currentDir);
				if( nei.hasPawn() && 
					nei.getPiece().getType() == Type.PAWN &&
					nei.getPiece().getColor()!=getColor() && 
					((Pawn)nei.getPiece()).hasJustMoved()) {
					
					Set<CheckerSquare> moves = new HashSet<>();
					moves.add(nei);
					attacks.put(currentDir, moves);
				}
			} catch (Exception e) {
			}
		}
	}


	@Override
	public Map<Direction, Set<CheckerSquare>> getFreeMovesByDirection() {
		Map<Direction,Set<CheckerSquare>> movesMap = new HashMap<>();
		Set<CheckerSquare> moves = new HashSet<>();

		try {
			Square square = getSquare().getNeighbor(color.getDirection());
			if (!square.hasPawn()) {
				moves.add((CheckerSquare) square);
				if (getRowPosition() == 2) {
					square = square.getNeighbor(color.getDirection());
					if (square.getPawns().size() == 0) {
						moves.add((CheckerSquare) square);
					}
				}
				movesMap.put(color.getDirection(), moves);
			}
		} catch (Exception e) {
			
		}
		return movesMap;
	}
}
