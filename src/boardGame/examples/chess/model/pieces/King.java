package boardGame.examples.chess.model.pieces;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import boardGame.api.model.Direction;
import boardGame.api.model.Square;
import boardGame.examples.chess.model.CheckerSquare;
import boardGame.examples.chess.model.Color;
import boardGame.examples.chess.model.Piece;
import boardGame.examples.chess.model.Type;

public class King extends Piece {

	public King(Color color) {
		super(Type.KING, color);
	}

	@Override
	public Map<Direction, Set<CheckerSquare>> getAttacksByDirection() {
		Map<Direction, Set<CheckerSquare>> attacks = new HashMap<>();

		for (Entry<Direction, Square> entry : getSquare().getNeighbors().entrySet()) {
			CheckerSquare square = (CheckerSquare) entry.getValue();
			Set<CheckerSquare> moves = new HashSet<>();
			if (square.hasPawn() && square.getPiece().getColor() != getColor()) {
				moves.add(square);
				attacks.put(entry.getKey(), moves);
			}
		}
		return attacks;
	}

	@Override
	public Map<Direction, Set<CheckerSquare>> getFreeMovesByDirection() {
		Map<Direction, Set<CheckerSquare>> movesMap = new HashMap<>();

		for (Entry<Direction, Square> entry : getSquare().getNeighbors().entrySet()) {
			CheckerSquare square = (CheckerSquare) entry.getValue();
			Set<CheckerSquare> moves = new HashSet<>();
			if (!square.hasPawn()) {
				moves.add(square);
				movesMap.put(entry.getKey(), moves);
			}
		}
		
		//TODO castle (fr: roque)
		
		return movesMap;
	}

}
