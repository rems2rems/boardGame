package boardGame.examples.chess.model.pieces;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import boardGame.api.model.Direction;
import boardGame.api.model.Square;
import boardGame.examples.chess.model.CheckerSquare;
import boardGame.examples.chess.model.Color;
import boardGame.examples.chess.model.Piece;
import boardGame.examples.chess.model.Type;

public class Knight extends Piece {

	public Knight(Color color) {
		super(Type.KNIGHT, color);
	}
	
	@Override
	public Set<CheckerSquare> getAllMoves() {
		Set<CheckerSquare> moves = new HashSet<>();
		for (Map.Entry<Direction, Square> entry : getSquare().getOrthogonalNeighbors().entrySet()) {
			try {
				Direction rightSide = entry.getKey().combineWith(entry.getKey().getClockWiseQuarter());
				CheckerSquare square = (CheckerSquare)entry.getValue().getNeighbor(rightSide);
				if(square!=null && (!square.hasPawn() || square.getPiece().getColor()!=getColor() )) {
					moves.add(square);
				}
			} catch (Exception e) {
			}
			try {
				Direction leftSide = entry.getKey().combineWith(entry.getKey().getAntiClockWiseQuarter());
				CheckerSquare square = (CheckerSquare)entry.getValue().getNeighbor(leftSide);
				if(!square.hasPawn() || square.getPiece().getColor()!=getColor()) {
					moves.add(square);
				}
			} catch (Exception e) {
			}
		}
		return moves;
	}
	
	@Override
	public Set<CheckerSquare> getFreeMoveSquares() {
		Set<CheckerSquare> moves = new HashSet<>();
		for (CheckerSquare square : getAllMoves()) {
			if(!square.hasPawn()) {
				moves.add(square);
			}
		}
		return moves;
	}
	
	@Override
	public Set<CheckerSquare> getAttacksSquares() {
		Set<CheckerSquare> moves = new HashSet<>();
		for (CheckerSquare square : getAllMoves()) {
			if(square.hasPawn() && square.getPiece().getColor() != getColor()) {
				moves.add(square);
			}
		}
		return moves;
	}
	
	@Override
	public Map<Direction, Set<CheckerSquare>> getAttacksByDirection() {
		
		throw new RuntimeException("knight does not have **direction**");
	}

	@Override
	public Map<Direction, Set<CheckerSquare>> getFreeMovesByDirection() {
		throw new RuntimeException("knight does not have **direction**");
	}
}
