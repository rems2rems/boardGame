package boardGame.examples.chess.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import boardGame.api.model.Direction;
import boardGame.model.PawnImpl;

public abstract class Piece extends PawnImpl {

	protected Type type;
	protected Color color;
	
	protected boolean neverMoved = true;
	protected boolean justMoved = false;
	
	public Piece(Type type,Color color) {
		this.type = type;
		this.color = color;
	}
	
	public Type getType() {
		return type;
	}
	public Color getColor() {
		return color;
	}
	
	abstract public Map<Direction,Set<CheckerSquare>> getAttacksByDirection();
	abstract public Map<Direction,Set<CheckerSquare>> getFreeMovesByDirection();
	
	public Map<Direction,Set<CheckerSquare>> getAllMovesByDirection(){
		Map<Direction,Set<CheckerSquare>> moves = new HashMap<>(getAttacksByDirection());
		moves.putAll(getFreeMovesByDirection());
		return moves;
	}
	
	public Set<CheckerSquare> getFreeMoveSquares(){
		Set<CheckerSquare> moves = new HashSet<>();
		for(Map.Entry<Direction, Set<CheckerSquare>> entry : getFreeMovesByDirection().entrySet()) {
			moves.addAll(entry.getValue());
		}
		return moves;
	}
	
	public Set<CheckerSquare> getAttacksSquares() {
		Set<CheckerSquare> moves = new HashSet<>();
		for(Map.Entry<Direction, Set<CheckerSquare>> entry : getAttacksByDirection().entrySet()) {
			moves.addAll(entry.getValue());
		}
		return moves;
	}
	
	public Set<CheckerSquare> getAllMoves() {
		Set<CheckerSquare> allMoves = new HashSet<>(getFreeMoveSquares());
		for (Map.Entry<Direction, Set<CheckerSquare>> entry : getAttacksByDirection().entrySet()) {
			allMoves.addAll(entry.getValue());
		}
		return allMoves; 
		
	}
	
	public int getRowPosition(){
		if(color == Color.WHITE) {
			return 8 - getSquare().getRow();
		}
		return getSquare().getRow() + 1;
	}
	
	public int getFile() {
		return getSquare().getColumn() + 1;
	}
	
	@Override
	public String toString() {
		return color.name().substring(0,1).toLowerCase() + type.name().substring(0,1);
	}
	
	public void focus() {
		((CheckerSquare)getSquare()).setSelectedPiece(true);
		for(CheckerSquare square : getFreeMoveSquares()) {
			square.setPossibleMove(true);
		}
		for(CheckerSquare square : getAttacksSquares()) {
			square.setPossibleAttack(true);
		}
	}
	
	public void unfocus() {
		((CheckerSquare)getSquare()).setSelectedPiece(false);
		for(CheckerSquare square : getFreeMoveSquares()) {
			square.setPossibleMove(false);
		}
		for(CheckerSquare square : getAttacksSquares()) {
			square.setPossibleAttack(false);
		}
	}
	
	public boolean hasPossibleMove() {
		return getAllMoves().size()>0;
	}
	

	public boolean hasNeverMoved() {
		return neverMoved;
	}

	public void setNeverMoved(boolean neverMoved) {
		this.neverMoved = neverMoved;
	}

	public boolean hasJustMoved() {
		return justMoved;
	}

	public void setJustMoved(boolean justMoved) {
		this.justMoved = justMoved;
	}
}
