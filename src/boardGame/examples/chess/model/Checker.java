package boardGame.examples.chess.model;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import boardGame.api.model.Direction;
import boardGame.api.model.Square;
import boardGame.examples.chess.model.pieces.Bishop;
import boardGame.examples.chess.model.pieces.King;
import boardGame.examples.chess.model.pieces.Knight;
import boardGame.examples.chess.model.pieces.Pawn;
import boardGame.examples.chess.model.pieces.Queen;
import boardGame.examples.chess.model.pieces.Rook;
import boardGame.model.BoardImpl;

public class Checker extends BoardImpl {

	public Checker() {
		super(8, 8, false, false,true);
		for (int row = 0; row < getNbRows(); row++) {
			for (int col = 0; col < getNbCols(); col++) {
				
				addSquare(new CheckerSquare(getCursor()), row, col);
			}
		}
		
		Piece rook1 = new Rook(Color.BLACK);
		getSquare(0, 0).addPawn(rook1);
		Piece rook2 = new Rook(Color.BLACK);
		getSquare(0, 7).addPawn(rook2);
		
		Piece knight1 = new Knight(Color.BLACK);
		getSquare(0, 1).addPawn(knight1);
		Piece knight2 = new Knight(Color.BLACK);
		getSquare(0, 6).addPawn(knight2);
		
		Piece bishop1 = new Bishop(Color.BLACK);
		getSquare(0, 2).addPawn(bishop1);
		Piece bishop2 = new Bishop(Color.BLACK);
		getSquare(0, 5).addPawn(bishop2);
		
		Piece queen = new Queen(Color.BLACK);
		getSquare(0, 3).addPawn(queen);
		
		Piece king = new King(Color.BLACK);
		getSquare(0, 4).addPawn(king);
		
		for (int i = 0; i < 8; i++) {
			Piece pawn = new Pawn(Color.BLACK);
			getSquare(1, i).addPawn(pawn);
		}
		
		
		rook1 = new Rook(Color.WHITE);
		getSquare(7, 0).addPawn(rook1);
		rook2 = new Rook(Color.WHITE);
		getSquare(7, 7).addPawn(rook2);
		
		knight1 = new Knight(Color.WHITE);
		getSquare(7, 1).addPawn(knight1);
		knight2 = new Knight(Color.WHITE);
		getSquare(7, 6).addPawn(knight2);
		
		bishop1 = new Bishop(Color.WHITE);
		getSquare(7, 2).addPawn(bishop1);
		bishop2 = new Bishop(Color.WHITE);
		getSquare(7, 5).addPawn(bishop2);
		
		queen = new Queen(Color.WHITE);
		getSquare(7, 3).addPawn(queen);
		
		king = new King(Color.WHITE);
		getSquare(7, 4).addPawn(king);
		
		for (int i = 0; i < 8; i++) {
			Piece pawn = new Pawn(Color.WHITE);
			getSquare(6, i).addPawn(pawn);
		}
		
		getCursor().move(getSquare(6, 5));
	}
	
	public Set<Piece> getPieces(Color color) {
		Set<Piece> pieces = new HashSet<>();
		for (Piece piece : (Set<Piece>)getPawns()) {
			if(piece.getColor() == color) {
				pieces.add(piece);
			}
		}
		return pieces;
	}
	public boolean isMate(Color currentColor) {
		
		if(!isCheck(currentColor)) {
			return false;
		}
		
		Set<Piece> pieces = getPieces(currentColor);
		Set<Piece> ennemies = getPieces(currentColor.getOpposite());
		Piece king = getKing(currentColor);
		Piece attacker = null;
		for (Piece ennemy : ennemies) {
			if(ennemy.getAttacksSquares().contains(king.getSquare())) {
				attacker = ennemy;
				break;
			}
		}
		
		// choice1: escape
		for (Square kingMove : king.getFreeMoveSquares()) {
			boolean hasEscape = true;
			for (Piece ennemy : ennemies) {
				if(ennemy.getAllMoves().contains(kingMove)) {
					hasEscape = false;
				}
			}
			if(hasEscape) {
				return false;
			}
		}
		
		// choice2: eat
		for (Piece piece : pieces) {
			if(piece.getAttacksSquares().contains(attacker.getSquare()) && isValidMove(piece, (CheckerSquare)attacker.getSquare())) {
				return false;
			}
		}
		
		//XXX choice3: break check
		if(attacker.getType() == Type.KNIGHT) {
			return true;
		}
		Set<CheckerSquare> path = getPath(king, attacker);
		for (Piece piece : pieces) {
			Set<CheckerSquare> intersection = new HashSet<>(piece.getAllMoves());
			intersection.retainAll(path);
			for (CheckerSquare square : intersection) {
				if(isValidMove(piece, square)) {
					return false;
				}
			}
		}
		return true;
	}

	public Set<CheckerSquare> getPath(Piece king, Piece attacker) {
		Set<CheckerSquare> path = new HashSet<>();
		for(Map.Entry<Direction, Set<CheckerSquare>> entry : attacker.getAttacksByDirection().entrySet()) {
			if(entry.getValue().contains(king.getSquare())) {
				return path;
			}
		}
		throw new RuntimeException("no path between " + king + " and " + attacker);
	}

	public boolean isCheck(Color currentColor) {
		Piece king = getKing(currentColor);
		for (Piece piece : getPieces(currentColor.getOpposite())) {
			if(piece.getAttacksSquares().contains(king.getSquare())) {
				return true;
			}
		}
		return false;
	}

	public Piece getKing(Color currentColor) {
		Piece otherKing = null;
		for (Piece piece : getPieces(currentColor)) {
			if(piece.getType() == Type.KING) {
				otherKing = piece;
			}
		}
		return otherKing;
	}
	
	public boolean isValidMove(Piece selectedPiece, CheckerSquare target) {
		if(!selectedPiece.getAllMoves().contains(target)) {
			return false;
		}
		CheckerSquare from = (CheckerSquare)selectedPiece.getSquare();
		Piece opponent=null;
		if(target.hasPawn()) {
			opponent = target.getPiece();
			target.removePawn(opponent);
		}
		selectedPiece.move(target);
		boolean isValid = !isCheck(selectedPiece.getColor());
		selectedPiece.move(from);
		if(opponent!=null) {
			target.addPawn(opponent);
		}
		
		return isValid;
	}
}
