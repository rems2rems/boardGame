package boardGame.examples.chess.controller;

import boardGame.api.controller.ControllerObserver;
import boardGame.examples.chess.model.CheckerSquare;
import boardGame.examples.chess.model.Color;
import boardGame.examples.chess.model.Piece;

public interface GameObserver extends ControllerObserver{

	void onPlay(Color color, Piece playedPiece, CheckerSquare to);
}
