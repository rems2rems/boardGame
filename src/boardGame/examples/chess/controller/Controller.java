package boardGame.examples.chess.controller;

import java.util.Random;

import boardGame.api.controller.ControllerObserver;
import boardGame.api.model.Direction;
import boardGame.api.model.Pawn;
import boardGame.examples.chess.Bot;
import boardGame.examples.chess.ChessPlay;
import boardGame.examples.chess.model.Checker;
import boardGame.examples.chess.model.CheckerSquare;
import boardGame.examples.chess.model.Color;
import boardGame.examples.chess.model.Piece;

public class Controller {

	private Checker board;
	private Pawn cursor;
	private Piece selectedPiece;
	private Color currentColor = Color.WHITE;

	private ControllerObserver observer;

	private Color humanColor = Color.values()[new Random().nextInt(Color.values().length)];

	private Bot bot;
	
	private Piece lastPiece = null;
	
	public Controller(Checker board) {
		super();
		this.board = board;
		cursor = board.getCursor();
	}

	public void moveCursor(Direction direction) {
		if (currentColor != humanColor) {
			return;
		}
		if (cursor.getSquare().getNeighbor(direction) != null) {
			cursor.move(direction);
		}
	}

	public void toggleSelect() {

		if (currentColor != humanColor) {
			return;
		}
		
		CheckerSquare cursorSquare = (CheckerSquare)cursor.getSquare();
				
		if (selectedPiece == null) {
			
			
			if (!cursorSquare.hasPawn() || cursorSquare.getPiece().getColor() != currentColor) {
				return;
			}
//			Piece piece = cursorSquare.getPiece();
//			if (piece.getColor() != currentColor) {
//				return;
//			}
			selectedPiece = cursorSquare.getPiece();
			selectedPiece.focus();
			System.out.println("select...");
			return;
		}
		if (cursor.getSquare() == selectedPiece.getSquare()) {
			selectedPiece.unfocus();
			selectedPiece = null;
			System.out.println("unselect.");
			return;
		}
		if (!board.isValidMove(selectedPiece, cursorSquare)) {
			System.out.println("invalid!");
			return;
		}
		selectedPiece.unfocus();
		plays(selectedPiece,cursorSquare);
		selectedPiece = null;
		System.out.println("you has played.");
		System.out.println(board);
		if (board.isMate(currentColor)) {
			observer.notifyGameIsOver("CheckMate! " + currentColor.getOpposite().name() + " wins !!!");
			return;
		}
		System.out.println("bot turn...");
		ChessPlay botPlay = bot.play();
		plays(botPlay.getFrom().getPiece(), botPlay.getTo());

		System.out.println("bot has played.");
		System.out.println(board);
		if (board.isMate(currentColor)) {
			observer.notifyGameIsOver("CheckMate! " + currentColor.getOpposite().name() + " wins !!!");
			return;
		}
		System.out.println("your turn...");
		
	}

	private void plays(Piece aPiece,CheckerSquare to) {
		
		if (to.hasPawn()) {
			Piece piece = to.getPiece();
			board.remove(piece);
		}
		
		aPiece.move(to);
		aPiece.setJustMoved(true);
		
		if(lastPiece!=null) {
			lastPiece.setJustMoved(false);
		}
		lastPiece = aPiece;
		
		//TODO: castle (fr: roque)
		
		//TODO: pawn promotion
		
		currentColor = currentColor.getOpposite();
	}

	public void addObserver(ControllerObserver observer) {
		this.observer = observer;
	}

	public void botStart() {
		ChessPlay botPlay = bot.play();
		plays(botPlay.getFrom().getPiece(), botPlay.getTo());
	}

	public Color getCurrentColor() {
		return currentColor;
	}

	public Color getHumanColor() {
		return humanColor;
	}

	public void setBot(Bot bot) {
		this.bot = bot;
	}
}
