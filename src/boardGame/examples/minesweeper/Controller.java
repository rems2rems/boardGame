package boardGame.examples.minesweeper;

import boardGame.api.controller.ControllerObserver;
import boardGame.api.model.Action;
import boardGame.api.model.Board;
import boardGame.api.model.Direction;
import boardGame.api.model.Pawn;
import boardGame.examples.minesweeper.model.Cell;

public class Controller {

	Board board;
	Pawn cursor;
	ControllerObserver observer;

	public Controller(Board board) {
		this.board = board;
		this.cursor = board.getCursor();
	}

	private boolean isGameOver() {
		boolean isGameOver = true;
		for (int row = 0; row < board.getNbRows(); row++) {
			for (int col = 0; col < board.getNbCols(); col++) {
				Cell square = (Cell) board.getSquare(row, col);
				if (square.isMined() && !square.isFlagged()) {
					isGameOver = false;
				}
			}
		}
		return isGameOver;
	}

	public void move(Direction direction) {
		Cell currentSquare = (Cell) cursor.getSquare();
		if (currentSquare.getNeighbors().keySet().contains(direction)) {
			cursor.move(direction);
		}
		System.out.println(board);
	}

	public void act(Action action) {
		Cell currentSquare = (Cell) cursor.getSquare();
		if (action == Action.ACTION1 && !currentSquare.isFlagged() && !currentSquare.isKnown()) {
			try {
				currentSquare.reveal();
			} catch (Exception e) {
				if(e.getMessage().equals("mined")) {
					observer.notifyGameIsOver("you lose!");
				}
			}
		}
		if ((action == Action.ACTION2 || action == Action.ACTION3) && !currentSquare.isKnown()) {
			currentSquare.toggleFlag();
			if (isGameOver()) {
				
				observer.notifyGameIsOver("you win!");
			}
		}

		System.out.println(board);
	}
	
	public void addObserver(ControllerObserver observer) {
		this.observer = observer;
	}
}
