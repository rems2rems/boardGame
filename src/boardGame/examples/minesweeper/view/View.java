package boardGame.examples.minesweeper.view;

import java.awt.BorderLayout;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

import boardGame.api.controller.ControllerObserver;
import boardGame.api.model.Action;
import boardGame.api.model.Board;
import boardGame.api.model.Direction;
import boardGame.api.model.Pawn;
import boardGame.api.model.Square;
import boardGame.api.view.PlayerObserver;
import boardGame.examples.minesweeper.Controller;
import boardGame.view.BoardPanel;
import boardGame.view.CursorView;
import boardGame.view.SimpleKeyboardManager;

public class View extends JFrame implements PlayerObserver, ControllerObserver {

	private static final long serialVersionUID = -5443051306016536158L;

	Set<Square> minables = new HashSet<>();

	Controller controller;
	BoardPanel game;

	public View(String title, Controller controller, Board board)  {
		super(title);
		this.controller = controller;
		controller.addObserver(this);
		int squareHeight = 50;
		int squareWidth = 50;
		game = new BoardPanel(board, squareHeight, squareWidth);
		for (int row = 0; row < board.getNbRows(); row++) {
			for (int col = 0; col < board.getNbCols(); col++) {
				game.addSquareView(new CellView(board.getSquare(row, col), squareHeight, squareWidth));
				for (Pawn pawn : board.getSquare(row, col).getPawns()) {
					game.addPawnView(new CursorView(pawn, squareHeight, squareWidth));
				}
			}
		}
		
		SimpleKeyboardManager keyboardManager = new SimpleKeyboardManager();
		addKeyListener(keyboardManager);
		keyboardManager.addObserver(0, this);

		setLayout(new BorderLayout());
		getContentPane().add(game);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		getContentPane().setSize(game.getSize());
		pack();
		setVisible(true);
	}

	@Override
	public void onAction(int playerNbr, Action action, Direction direction) {

		if (action == Action.MOVE) {
			controller.move(direction);
			return;
		}
		controller.act(action);
	}

	@Override
	public void notifyGameIsOver(String message) {
		
		JOptionPane.showMessageDialog(game, message);
		System.exit(0);
	}
}
