package boardGame.examples.minesweeper.model;

import java.util.HashSet;
import java.util.Set;

import boardGame.api.model.Board;
import boardGame.model.BoardImpl;

public class FieldBuilder {

	private Board board;
	
	public Board getBoard() {
		return board;
	}

	public FieldBuilder(int nbRows, int nbCols) {
		board = new BoardImpl(nbRows, nbCols, false, false,true);
		for (int row = 0; row < board.getNbRows(); row++) {
			for (int col = 0; col < board.getNbCols(); col++) {
				
				board.addSquare(new Cell(board.getCursor()), row, col);
			}
		}
	}
	

	public FieldBuilder addMines(int nbMines) {
		setMines(pickMines(nbMines));
		return this;
	}
	
	public Set<Cell> pickMines(int nbMines) {

		Set<Cell> mines = new HashSet<>();
				
		while (mines.size() < nbMines) {
			Cell mine = (Cell)board.pickRandomSquare();
			if(mines.contains(mine)) {
				continue;
			}
			mines.add(mine);
		}
		return mines;
	}
	
	public FieldBuilder setMines(Set<Cell> mines) {
		for (Cell cell : mines) {
			cell.setMined(true);
		}
		return this;
	}
}
