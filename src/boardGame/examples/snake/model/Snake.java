package boardGame.examples.snake.model;

import java.util.LinkedList;
import java.util.Map;

import boardGame.api.model.Board;
import boardGame.api.model.Direction;
import boardGame.api.model.Pawn;
import boardGame.api.model.Square;

public class Snake {

    private LinkedList<SnakePart> parts = new LinkedList<>();
    
    public LinkedList<SnakePart> getParts() {
		return parts;
	}

	private Direction direction;
    
    public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public Snake(Board board) {
		Square square = board.pickRandomSquare();
		SnakePart head = new SnakeHead();
		
		parts.add(head);
		square.addPawn(head);
		square = square.pickRandomOrthogonalNeighbor().getValue();
		SnakePart second = new SnakePart();
		parts.add(second);
		square.addPawn(second);
		square = square.pickRandomOrthogonalNeighbor().getValue();
		SnakePart third = new SnakePart();
		parts.add(third);
		square.addPawn(third);
		
		for(Map.Entry<Direction,Square> neigh : head.getSquare().getOrthogonalNeighbors().entrySet()) {
			if(neigh.getValue().getPawns().size() > 0) {
				continue;
			}
			direction = neigh.getKey();
		}
		
	}
    
    public void eat(Food food) {
    	Square firstSquare = food.getSquare();
    	Square secondSquare = getHead().getSquare();
    	SnakePart newPart = new SnakePart();
    	secondSquare.addPawn(newPart);
    	getHead().move(firstSquare);
    	SnakePart head = parts.removeFirst();
    	parts.addFirst(newPart);
    	parts.addFirst(head);
    }
    
    public Pawn getHead() {
    	return parts.getFirst();
    }
    
    public void move(Square square) {
    	Square next = square;
    	for(Pawn part : parts) {
    		Square previous = part.getSquare();
    		part.move(next);
    		next = previous;
    	}
    }
}
