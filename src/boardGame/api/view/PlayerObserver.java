package boardGame.api.view;

import boardGame.api.model.Action;
import boardGame.api.model.Direction;

public interface PlayerObserver {

	public void onAction(int playerNbr,Action action,Direction direction);
}
