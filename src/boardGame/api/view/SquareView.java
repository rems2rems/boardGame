package boardGame.api.view;

import boardGame.api.model.Square;
import boardGame.api.model.SquareObserver;

public interface SquareView extends Drawable,SquareObserver {
	
	Square getSquare();
	void addObserver(SquareViewObserver observer);
	void removeObserver(SquareViewObserver observer);
}
