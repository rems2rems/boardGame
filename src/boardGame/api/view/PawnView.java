package boardGame.api.view;

import boardGame.api.model.Pawn;
import boardGame.api.model.PawnObserver;

public interface PawnView extends Drawable,PawnObserver{

	Pawn getPawn();
	void addObserver(PawnViewObserver observer);
	void removeObserver(PawnViewObserver observer);
	
}
