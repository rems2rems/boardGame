package boardGame.api.view;

import java.awt.Color;
import java.awt.Image;

public interface Drawable {

	Image getSprite();
	void setSprite(Image sprite);
	boolean hasSprite();
	
	Color getColor();
	void setColor(Color color);

}