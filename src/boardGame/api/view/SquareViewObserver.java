package boardGame.api.view;

public interface SquareViewObserver {

	void onChange(SquareView squareView);
}
