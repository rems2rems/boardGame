package boardGame.api.model;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public enum Direction {
 
	UP(-1,0,true),UP_RIGHT(-1,1,false),RIGHT(0,1,true),DOWN_RIGHT(1,1,false),DOWN(1,0,true),DOWN_LEFT(1,-1,false),LEFT(0,-1,true),UP_LEFT(-1,-1,false);
	
	private int rowInc;
	private int colInc;
	private boolean isOrthogonal = true;
	
	public boolean isOrthogonal() {
		return isOrthogonal;
	}

	public int getRowInc() {
		return rowInc;
	}

	public int getColInc() {
		return colInc;
	}

	public Direction getOpposite(){
		switch (this) {
		case UP:
			return Direction.DOWN;
		case UP_RIGHT:
			return Direction.DOWN_LEFT;
		case RIGHT:
			return Direction.LEFT;
		case DOWN_RIGHT:
			return Direction.UP_LEFT;
		case DOWN:
			return Direction.UP;
		case DOWN_LEFT:
			return Direction.UP_RIGHT;
		case LEFT:
			return Direction.RIGHT;
		case UP_LEFT:
			return Direction.DOWN_RIGHT;
		default:
			throw new RuntimeException();
		}
	}
	
	public Direction getClockWiseQuarter(){
		switch (this) {
		case UP:
			return Direction.RIGHT;
		case UP_RIGHT:
			return Direction.DOWN_RIGHT;
		case RIGHT:
			return Direction.DOWN;
		case DOWN_RIGHT:
			return Direction.DOWN_LEFT;
		case DOWN:
			return Direction.LEFT;
		case DOWN_LEFT:
			return Direction.UP_LEFT;
		case LEFT:
			return Direction.UP;
		case UP_LEFT:
			return Direction.UP_RIGHT;
		default:
			throw new RuntimeException();
		}
	}
	
	public Direction getAntiClockWiseQuarter(){
		return getClockWiseQuarter().getOpposite();
	}
	
	public static Set<Direction> getOrthogonals() {
		Set<Direction> orthos = new HashSet<>();
		for (Direction direction : values()) {
			if(direction.isOrthogonal()) {
				orthos.add(direction);
			}
		}
		return orthos;
	}
	
	public static Set<Direction> getDiagonals() {
		Set<Direction> diags = new HashSet<>();
		for (Direction direction : values()) {
			if(!direction.isOrthogonal()) {
				diags.add(direction);
			}
		}
		return diags;
	}
	
	private Direction(int rowInc,int colInc,boolean isOrthogonal) {
		this.rowInc = rowInc;
		this.colInc = colInc;
		this.isOrthogonal = isOrthogonal;
	}
	
	public static Direction pickRandom() {
		Random random = new Random(System.currentTimeMillis());
		return values()[random.nextInt(values().length)];
	}
	
	public static Direction pickRandomDiagonal() {
		Random random = new Random(System.currentTimeMillis());
		Set<Direction> diags = getDiagonals();
		Direction[] diagonals = diags.toArray(new Direction[0]);
		return diagonals[random.nextInt(diagonals.length)];
	}
	
	public static Direction pickRandomOrthogonal() {
		Random random = new Random(System.currentTimeMillis());
		Set<Direction> orthos = getOrthogonals();
		Direction[] orthogonals = orthos.toArray(new Direction[0]);
		return orthogonals[random.nextInt(orthogonals.length)];
	}
	
	public boolean isDiagonal() {
		return !isOrthogonal();
	}
	
	public Direction combineWith(Direction other) {
		if(!isOrthogonal() || !other.isOrthogonal() || this == other || this == other.getOpposite()) {
			throw new RuntimeException("only orthogonal directions can be combined");
		}
		if(this==UP || this ==DOWN) {
			return Direction.valueOf(this.name() + "_" + other.name());
		}
		return Direction.valueOf(other.name() + "_" + this.name());
	}
}
