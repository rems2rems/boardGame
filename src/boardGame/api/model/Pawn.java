package boardGame.api.model;

public interface Pawn /*extends Textable*/ {
    
    Square getSquare();
    void setSquare(Square square);
    
    void move(Direction direction);
    void move(Square to);

	void addObserver(PawnObserver observer);
	void removeObserver(PawnObserver observer);
}
