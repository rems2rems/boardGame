package boardGame.api.model;

import java.util.Map;
import java.util.Set;

public interface Square /*extends Textable*/ {

	int getColumn();
	void setColumn(int column);
	
	int getRow();
	void setRow(int row);
	

	Set<? extends Pawn> getPawns();
	boolean hasPawn();
	void addPawn(Pawn pawn);
	void removePawn(Pawn pawn);
	
	void setCursor(Pawn pawn);
	
	void setNeighbor(Direction direction, Square neighbor);
	void removeNeighbor(Direction direction);
	Square getNeighbor(Direction direction);
	Map<Direction,Square> getNeighbors();
	Map<Direction,Square> getOrthogonalNeighbors();
	Map<Direction,Square> getDiagonalNeighbors();
	
	void addObserver(SquareObserver observer);
	void removeObserver(SquareObserver observer);
	
	Map.Entry<Direction,Square> pickRandomNeighbor();
	Map.Entry<Direction,Square> pickRandomOrthogonalNeighbor();
	Map.Entry<Direction,Square> pickRandomDiagonalNeighbor();
}
