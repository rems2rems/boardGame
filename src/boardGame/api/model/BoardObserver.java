package boardGame.api.model;

public interface BoardObserver extends SquareObserver,PawnObserver {

	void notifyGameIsOver();

}
