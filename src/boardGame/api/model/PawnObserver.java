package boardGame.api.model;

public interface PawnObserver {

	void onMove(Pawn source, Square from, Square to);
	void onAction(Pawn source, Action action);
}
