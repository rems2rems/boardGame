package boardGame.api.model;

import java.util.Set;

public interface Board {

	int getNbRows();
	int getNbCols();
	
    void addSquare(Square square, int column, int row);
    void removeSquare(int column, int row);
    Square getSquare(int row,int col);
    void add(Pawn pawn,Square to);
    void move(Pawn pawn,Direction direction);
    void move(Pawn pawn,Square to);
    void remove(Pawn pawn);
    
	void addObserver(BoardObserver observer);
	void removeObserver(BoardObserver observer);
	
	public Square pickRandomSquare(Set<Square> set);
	public Square pickRandomSquare();
	
	public Set<? extends Pawn> getPawns();

	public boolean hasCursor();
	public Pawn getCursor();
}
