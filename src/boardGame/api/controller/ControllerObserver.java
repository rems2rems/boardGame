package boardGame.api.controller;

public interface ControllerObserver {
	
	void notifyGameIsOver(String message);

}
