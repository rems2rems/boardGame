package boardGame.view;

import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import boardGame.api.model.Pawn;

public class CursorView extends PawnViewImpl {

	private static final long serialVersionUID = -7787163574042740696L;

	public CursorView(Pawn pawn,int squareHeight,int squareWidth) {
		super(pawn,squareHeight,squareWidth);
		try {
			Image sprite = ImageIO.read(new File("images/common/cursor.png"));
			setSprite(sprite);
			
		} catch (IOException e) {
			throw new RuntimeException("sprite not found");
		}
	}
}
