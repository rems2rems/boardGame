package boardGame.view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JComponent;

import boardGame.api.model.Pawn;
import boardGame.api.model.Square;
import boardGame.api.view.SquareView;
import boardGame.api.view.SquareViewObserver;

public class SquareViewImpl extends JComponent implements SquareView {

	private static final long serialVersionUID = -4788813817893947916L;
	
	protected Square square;
	private Color color = Color.WHITE;
	protected int squareHeight,squareWidth;
	
	public Square getSquare() {
		return square;
	}

	private Image sprite;
	{
		try {
			sprite = ImageIO.read(new File("images/minesweeper/empty.png"));
		} catch (IOException e) {
			throw new RuntimeException("sprite not found");
		}	
	}
	
	protected List<SquareViewObserver> observers = new ArrayList<>();

	public SquareViewImpl(Square square,int squareHeight,int squareWidth) {
		this.square = square;
		this.squareHeight = squareHeight;
		this.squareWidth = squareWidth;
		
		if(square != null) {
			square.addObserver(this);
		}
	}
	
	@Override
	public Image getSprite() {
		return sprite;
	}

	@Override
	public void onPawnAdded(Square source, Pawn pawn) {
		onStateChanged(source);
	}

	@Override
	public void onPawnRemoved(Square source, Pawn pawn) {
		onStateChanged(source);
	}

	@Override
	public void onStateChanged(Square source) {
		for (SquareViewObserver squareViewObserver : observers) {
			squareViewObserver.onChange(this);
		}
	}
	
	@Override
	public void addObserver(SquareViewObserver observer) {
		observers.add(observer);
	}

	@Override
	public void removeObserver(SquareViewObserver observer) {
		observers.remove(observer);
	}

	@Override
	public boolean hasSprite() {
		return sprite!=null;
	}

	@Override
	public Color getColor() {
		return color;
	}

	@Override
	public void setColor(Color color) {
		this.color = color;
	}

	@Override
	public void setSprite(Image sprite) {
		this.sprite = sprite;
	}
	
	@Override
	public void paint(Graphics g) {
		Graphics2D graphics = (Graphics2D)g;
		
		graphics.setPaint(getColor());
		graphics.fillRect(getSquare().getColumn()*squareWidth,getSquare().getRow()*squareHeight,squareWidth,squareHeight);
		
		if(getSprite()!=null) {
			graphics.drawImage(getSprite(),getSquare().getColumn()*squareWidth,getSquare().getRow()*squareHeight,squareWidth,squareHeight,this);
		}
		
	}
}
