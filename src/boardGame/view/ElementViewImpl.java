package boardGame.view;

import java.awt.Color;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import boardGame.api.model.Pawn;
import boardGame.api.model.Square;
import boardGame.api.view.SquareView;
import boardGame.api.view.SquareViewObserver;

public class ElementViewImpl implements SquareView {

	protected Square square;
	private Color color = Color.WHITE;
	
	public Square getSquare() {
		return square;
	}

	private Image sprite;
	{
		try {
			sprite = ImageIO.read(new File("images/minesweeper/empty.png"));
		} catch (IOException e) {
			throw new RuntimeException("sprite not found");
		}	
	}
	
	protected List<SquareViewObserver> observers = new ArrayList<>();

	public ElementViewImpl(Square square) {
		this.square = square;
		if(square != null) {
			square.addObserver(this);
		}
	}
	
	@Override
	public Image getSprite() {
		return sprite;
	}

	@Override
	public void onPawnAdded(Square source, Pawn pawn) {
		onStateChanged(source);
	}

	@Override
	public void onPawnRemoved(Square source, Pawn pawn) {
		onStateChanged(source);
	}

	@Override
	public void onStateChanged(Square source) {
		for (SquareViewObserver squareViewObserver : observers) {
			squareViewObserver.onChange(this);
		}
	}
	
	@Override
	public void addObserver(SquareViewObserver observer) {
		observers.add(observer);
	}

	@Override
	public void removeObserver(SquareViewObserver observer) {
		observers.remove(observer);
	}

	@Override
	public boolean hasSprite() {
		return sprite!=null;
	}

	@Override
	public Color getColor() {
		return color;
	}

	@Override
	public void setColor(Color color) {
		this.color = color;
	}

	@Override
	public void setSprite(Image sprite) {
		this.sprite = sprite;
	}
}
