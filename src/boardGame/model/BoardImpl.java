package boardGame.model;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import boardGame.api.model.Action;
import boardGame.api.model.Board;
import boardGame.api.model.BoardObserver;
import boardGame.api.model.Direction;
import boardGame.api.model.Pawn;
import boardGame.api.model.PawnObserver;
import boardGame.api.model.Square;
import boardGame.api.model.SquareObserver;

public class BoardImpl implements Board, SquareObserver, PawnObserver {

	private Square[][] squares;
	Pawn cursor;
	private boolean loopedHorizontally;
	private boolean loopedVertically;
	private Set<BoardObserver> observers = new HashSet<>();

	public BoardImpl() {
		this(10, 10, false, false,false);
	}

	public BoardImpl(int nbRows, int nbCols, boolean loopedHorizontally, boolean loopedVertically,boolean hasCursor) {
		squares = new Square[nbRows][nbCols];
		this.loopedHorizontally = loopedHorizontally;
		this.loopedVertically = loopedVertically;
		if(hasCursor) {
			cursor = new PawnImpl();
		}
		for (int row = 0; row < nbRows; row++) {
			for (int col = 0; col < nbCols; col++) {
				Square square = new SquareImpl(getCursor());
				addSquare(square, row, col);
				if(hasCursor) {
					square.setCursor(cursor);
				}
			}
		}
		if(hasCursor) {
			cursor.setSquare(squares[0][0]);
		}
	}

	@Override
	public void addSquare(Square square, int row, int column) {
		square.setColumn(column);
		square.setRow(row);
		if(squares[row][column] != null) {
			removeSquare(row, column);
		}
		squares[row][column] = square;
		square.addObserver(this);
		linkSquare(square);
	}

	public void linkSquare(Square square) {
		
		for (Direction direction : Direction.values()) {
			int neighborRow = Math.floorMod((square.getRow() + direction.getRowInc()), getNbRows());
			int neighborCol = Math.floorMod((square.getColumn() + direction.getColInc()), getNbCols());
			Square neighbor = getSquare(neighborRow, neighborCol);
			if (neighbor != null) {
				neighbor.setNeighbor(direction.getOpposite(), square);
				square.setNeighbor(direction, neighbor);
			}
		}
		if (!loopedVertically) {
			if (square.getRow() == 0) {
				try {
					square.getNeighbor(Direction.UP_LEFT).removeNeighbor(Direction.DOWN_RIGHT);
					square.removeNeighbor(Direction.UP_LEFT);
				} catch (Exception e) {
					//no neighbor
				}
				try {
					square.getNeighbor(Direction.UP).removeNeighbor(Direction.DOWN);
					square.removeNeighbor(Direction.UP);
				} catch (Exception e) {
					//no neighbor
				}
				try {
					square.getNeighbor(Direction.UP_RIGHT).removeNeighbor(Direction.DOWN_LEFT);
					square.removeNeighbor(Direction.UP_RIGHT);
				} catch (Exception e) {
					//no neighbor
				}
			}
			if (square.getRow() == getNbRows() - 1) {
				try {
					square.getNeighbor(Direction.DOWN_LEFT).removeNeighbor(Direction.UP_RIGHT);
					square.removeNeighbor(Direction.DOWN_LEFT);
				} catch (Exception e) {
					//no neighbor
				}
				try {
					square.getNeighbor(Direction.DOWN).removeNeighbor(Direction.UP);
					square.removeNeighbor(Direction.DOWN);
				} catch (Exception e) {
					//no neighbor
				}
				try {
					square.getNeighbor(Direction.DOWN_RIGHT).removeNeighbor(Direction.UP_LEFT);
					square.removeNeighbor(Direction.DOWN_RIGHT);
				} catch (Exception e) {
					//no neighbor
				}
			}
		}
		if (!loopedHorizontally) {
			if (square.getColumn() == 0) {
				try {
					square.getNeighbor(Direction.UP_LEFT).removeNeighbor(Direction.DOWN_RIGHT);
					square.removeNeighbor(Direction.UP_LEFT);
				} catch (Exception e) {
					//no neighbor
				}
				try {
					square.getNeighbor(Direction.LEFT).removeNeighbor(Direction.RIGHT);
					square.removeNeighbor(Direction.LEFT);
				} catch (Exception e) {
					//no neighbor
				}
				try {
					square.getNeighbor(Direction.DOWN_LEFT).removeNeighbor(Direction.UP_RIGHT);
					square.removeNeighbor(Direction.DOWN_LEFT);
				} catch (Exception e) {
					//no neighbor
				}
			}
			if (square.getColumn() == getNbCols() - 1) {
				try {
					square.getNeighbor(Direction.UP_RIGHT).removeNeighbor(Direction.DOWN_LEFT);
					square.removeNeighbor(Direction.UP_RIGHT);
				} catch (Exception e) {
					//no neighbor
				}
				try {
					square.getNeighbor(Direction.RIGHT).removeNeighbor(Direction.LEFT);
					square.removeNeighbor(Direction.RIGHT);
				} catch (Exception e) {
					//no neighbor
				}
				try {
					square.getNeighbor(Direction.DOWN_RIGHT).removeNeighbor(Direction.UP_LEFT);
					square.removeNeighbor(Direction.DOWN_RIGHT);
				} catch (Exception e) {
					//no neighbor
				}
			}
		}
	}
	
	@Override
	public Square getSquare(int row, int column) {
		return squares[row][column];
	}

	@Override
	public void addObserver(BoardObserver observer) {
		observers.add(observer);
	}

	@Override
	public void removeObserver(BoardObserver observer) {
		observers.remove(observer);
	}

	@Override
	public void add(Pawn pawn, Square to) {
		to.addPawn(pawn);
	}

	@Override
	public void move(Pawn pawn, Direction direction) {
		pawn.move(direction);
	}

	@Override
	public void move(Pawn pawn, Square to) {
		pawn.move(to);
	}

	@Override
	public void remove(Pawn pawn) {
		pawn.getSquare().removePawn(pawn);
	}

	@Override
	public void onMove(Pawn source, Square from, Square to) {
		for (BoardObserver observer : observers) {
			observer.onMove(source, from, to);
		}
	}

	@Override
	public void onPawnAdded(Square source, Pawn pawn) {
		for (BoardObserver observer : observers) {
			observer.onPawnAdded(source, pawn);
		}
	}

	@Override
	public void onPawnRemoved(Square source, Pawn pawn) {
		for (BoardObserver observer : observers) {
			observer.onPawnRemoved(source, pawn);
		}
	}

	@Override
	public void removeSquare(int row, int column) {

		Square toBeDeleted = getSquare(row,column);
		for (Map.Entry<Direction, Square> neighborEntry : toBeDeleted.getNeighbors().entrySet()) {
			Square neighbor = neighborEntry.getValue();
			Direction neighborDirection = neighborEntry.getKey();
			neighbor.removeNeighbor(neighborDirection.getOpposite());
		}
		squares[row][column] = null;
	}

	@Override
	public int getNbRows() {
		return squares.length;
	}

	@Override
	public int getNbCols() {
		return squares[0].length;
	}


	public Square pickRandomSquare(Set<Square> set){
		Random random = new Random(System.currentTimeMillis());
		return set.toArray(new Square[0])[random.nextInt(set.size())];
	}
	

    @Override
    public void onStateChanged(Square source) {
        for (BoardObserver observer : observers) {
               observer.onStateChanged(source);
        }
    }

	@Override
	public Square pickRandomSquare() {
		Set<Square> all = new HashSet<>();
		for (Square[] tmpSquares : squares) {
			all.addAll(Arrays.asList(tmpSquares));
		}
		return pickRandomSquare(all);
	}

	@Override
	public void onAction(Pawn source, Action action) {
		for (BoardObserver observer : observers) {
			observer.onAction(source, action);
		}
	}
	
	@Override
	public String toString() {
		String res = "";
		for (Square[] line : squares) {
			res += "|";
			for (Square square : line) {
				String sq = square.toString();
				for (Pawn pawn : square.getPawns()) {
					sq = pawn.toString();
				}
				res += sq;
			}
			res += "|\n";
		}
		return res;
	}

	@Override
	public Set<? extends Pawn> getPawns() {
		Set<Pawn> pawns = new HashSet<>();
		for (int row = 0; row < getNbRows(); row++) {
			for (int col = 0; col < getNbCols(); col++) {
				pawns.addAll(getSquare(row, col).getPawns());
			}
		}
		if(hasCursor()) {
			pawns.remove(getCursor());
		}
		return pawns;
	}

	@Override
	public boolean hasCursor() {
		return cursor != null;
	}

	@Override
	public Pawn getCursor() {
		return cursor;
	}	
}
