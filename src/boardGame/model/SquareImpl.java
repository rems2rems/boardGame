package boardGame.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import boardGame.api.model.Direction;
import boardGame.api.model.Pawn;
import boardGame.api.model.Square;
import boardGame.api.model.SquareObserver;

public class SquareImpl implements Square {

	private int row;
	private int column;
	private Set<Pawn> pawns = new HashSet<>();
	private Pawn cursor;
		
	public SquareImpl(Pawn cursor) {
		this.cursor = cursor;
	}

	protected Set<SquareObserver> observers = new LinkedHashSet<>();

	private Map<Direction, Square> neighbors = new HashMap<>();
	
	@Override
	public int getColumn() {
		return column;
	}

	@Override
	public void setColumn(int column) {
		this.column = column;
		
	}

	@Override
	public int getRow() {
		return row;
	}

	@Override
	public void setRow(int row) {
		this.row = row;
		
	}

	@Override
	public Set<? extends Pawn> getPawns() {
		Set<Pawn> pawns = new HashSet<>(this.pawns);
		pawns.remove(cursor);
		return pawns;
	}

	@Override
	public void addPawn(Pawn pawn) {
		pawns.add(pawn);
		pawn.setSquare(this);
		for (SquareObserver observer : observers) {
			observer.onPawnAdded(this, pawn);
		}
	}

	@Override
	public void removePawn(Pawn pawn) {
		pawns.remove(pawn);
		for (SquareObserver observer : observers) {
			observer.onPawnRemoved(this, pawn);
		}
	}

	@Override
	public Square getNeighbor(Direction direction) {
		return neighbors.get(direction);
	}

	@Override
	public void setNeighbor(Direction direction, Square neighbor) {
		neighbors.put(direction, neighbor);
	}

	@Override
	public void addObserver(SquareObserver observer) {
		observers.add(observer);
	}

	@Override
	public void removeObserver(SquareObserver observer) {
		observers.remove(observer);
	}

	@Override
	public Map<Direction,Square> getNeighbors() {
		return neighbors;
	}
	
	@Override
	public Map.Entry<Direction,Square> pickRandomNeighbor() {
		Random random = new Random(System.currentTimeMillis());
		Set<Map.Entry<Direction,Square>> choicesSet = getNeighbors().entrySet();
		@SuppressWarnings("unchecked")
		Map.Entry<Direction,Square>[] choices = (Map.Entry<Direction,Square>[])choicesSet.toArray(new Map.Entry[0]);
		return choices[random.nextInt(choices.length)];
	}

	@Override
	public Entry<Direction, Square> pickRandomOrthogonalNeighbor() {
		Random random = new Random(System.currentTimeMillis());
		Set<Map.Entry<Direction,Square>> choicesSet = getOrthogonalNeighbors().entrySet();
		@SuppressWarnings("unchecked")
		Map.Entry<Direction,Square>[] choices = (Map.Entry<Direction,Square>[])choicesSet.toArray(new Map.Entry[0]);
		return choices[random.nextInt(choices.length)];
	}

	@Override
	public Entry<Direction, Square> pickRandomDiagonalNeighbor() {
		Random random = new Random(System.currentTimeMillis());
		Set<Map.Entry<Direction,Square>> choicesSet = getDiagonalNeighbors().entrySet();
		@SuppressWarnings("unchecked")
		Map.Entry<Direction,Square>[] choices = (Map.Entry<Direction,Square>[])choicesSet.toArray(new Map.Entry[0]);
		return choices[random.nextInt(choices.length)];
	}

	@Override
	public Map<Direction, Square> getOrthogonalNeighbors() {
		Map<Direction, Square> allNeighbors = getNeighbors();
		Map<Direction, Square> neighbors = new HashMap<>();
		for (Map.Entry<Direction, Square> neigh : allNeighbors.entrySet()) {
			if(neigh.getKey().isOrthogonal()) {
				neighbors.put(neigh.getKey(), neigh.getValue());
			}
		}
		return neighbors;
	}
	
	@Override
	public Map<Direction, Square> getDiagonalNeighbors() {
		Map<Direction, Square> allNeighbors = getNeighbors();
		Map<Direction, Square> neighbors = new HashMap<>();
		for (Map.Entry<Direction, Square> neigh : allNeighbors.entrySet()) {
			if(neigh.getKey().isDiagonal()) {
				neighbors.put(neigh.getKey(), neigh.getValue());
			}
		}
		return neighbors;
	}

	@Override
	public void removeNeighbor(Direction direction) {
		neighbors.remove(direction);
	}
	
	@Override
	public String toString() {
		return " ";
	}

	@Override
	public void setCursor(Pawn pawn) {
		this.cursor = pawn;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + column;
		result = prime * result + row;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SquareImpl other = (SquareImpl) obj;
		if (column != other.column)
			return false;
		if (row != other.row)
			return false;
		return true;
	}

	@Override
	public boolean hasPawn() {
		
		return getPawns().size()>0;
	}
}
