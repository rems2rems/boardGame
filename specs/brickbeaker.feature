
Feature: Chess Game
  I want to test the pieces moves

	Scenario: pawn normal move
    Given I have a checker
      | _  | _ | _ |
      | _  | _ | _ |
      | _  | _ | _ |
      | Pb | _ | _ |
    When i move a pawn
    Then the checker is like that
      | _  | _ | _ |
      | _  | _ | _ |
      | Pb | _ | _ |
      | _  | _ | _ |
      
    Scenario: pawn eats
    Given I have a checker
    When i move a pawn to eat something
      | _  | _  | _ |
      | _  | _  | _ |
      | _  | bP | _ |
      | wP | _  | _ |
    Then the checker is like that
      | _ | _  | _ |
      | _ | _  | _ |
      | _ | wP | _ |
      | _ | _  | _ |