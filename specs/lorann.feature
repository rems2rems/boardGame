
Feature: MineSweeper
  I want to test the minesweeper behavior

	Scenario Outline: count middle mines
    When I have a field
      | _ | _ | _ |
      | _ | X | _ |
      | _ | _ | _ |
    Then cell at <row> and <column> has <nbMines> neighbor mines
    
    Examples:
    	| row | column | nbMines |
    	| 0   | 0      | 1       |
    	| 0   | 1      | 1       |
    	| 0   | 2      | 1       |
    	| 1   | 0      | 1       |
    	| 1   | 2      | 1       |
    	| 2   | 0      | 1       |
    	| 2   | 1      | 1       |
    	| 2   | 2      | 1       |
    	
  Scenario Outline: count border vertical mines
    When I have a field
      | _ | _ | _ | _ |
      | _ | _ | _ | _ |
      | X | _ | _ | _ |
      | X | _ | _ | _ |
    Then cell at <row> and <column> has <nbMines> neighbor mines
    
    Examples:
    	| row | column | nbMines |
    	| 0   | 0      | 0       |
    	| 0   | 1      | 0       |
    	| 0   | 2      | 0       |
    	| 0   | 3      | 0       |
    	| 1   | 0      | 1       |
    	| 1   | 1      | 1       |
    	| 1   | 2      | 0       |
    	| 1   | 3      | 0       |
    	| 2   | 1      | 2       |
    	| 2   | 2      | 0       |
    	| 2   | 3      | 0       |
    	| 3   | 1      | 2       |
    	| 3   | 2      | 0       |
    	| 3   | 3      | 0       |
    	
    Scenario Outline: count border vertical mines 2
    When I have a field
      | _ | _ | _ | _ |
      | _ | _ | _ | _ |
      | _ | _ | _ | X |
      | _ | _ | _ | X |
    Then cell at <row> and <column> has <nbMines> neighbor mines
    
    Examples:
    	| row | column | nbMines |
    	| 0   | 0      | 0       |
    	| 0   | 1      | 0       |
    	| 0   | 2      | 0       |
    	| 0   | 3      | 0       |
    	| 1   | 0      | 0       |
    	| 1   | 1      | 0       |
    	| 1   | 2      | 1       |
    	| 1   | 3      | 1       |
    	| 2   | 0      | 0       |
    	| 2   | 1      | 0       |
    	| 2   | 2      | 2       |
    	| 3   | 0      | 0       |
    	| 3   | 1      | 0       |
    	| 3   | 2      | 2       |
    	
    Scenario Outline: count border horizontal mines
    When I have a field
      | _ | _ | _ |
      | _ | _ | _ |
      | X | X | _ |
    Then cell at <row> and <column> has <nbMines> neighbor mines
    
    Examples:
    	| row | column | nbMines |
    	| 0   | 0      | 0       |
    	| 0   | 1      | 0       |
    	| 0   | 2      | 0       |
    	| 1   | 0      | 2       |
    	| 1   | 1      | 2       |
    	| 1   | 2      | 1       |
    	| 2   | 2      | 1       |
